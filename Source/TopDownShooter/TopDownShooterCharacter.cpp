// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TDSGameInstance.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "ProjectileDefault.h"



#include "Engine/World.h"
 
ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	
	// Create a components
	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UTDSCharacterHealthComponent>(TEXT("HealthComponent"));

	//Delegates

	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &ATopDownShooterCharacter::CharDead);
	}


	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATopDownShooterCharacter::InitWeapon);
	}

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true; 
}


void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = UGameplayStatics::GetPlayerController(this, 0);
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	
	MovementTick(DeltaSeconds);

}


void ATopDownShooterCharacter::BeginPlay()
{
	Super::BeginPlay();


	
	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}


void ATopDownShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATopDownShooterCharacter::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATopDownShooterCharacter::InputAxisY);

	PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::InputAttackPressed);
	PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::InputAttackReleased);
	PlayerInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::TryReloadWeapon);

	PlayerInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::TrySwitchNextWeapon);
	PlayerInputComponent->BindAction(TEXT("SwitchPrevWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::TrySwitchPrevWeapon);
	PlayerInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::DropCurrentWeapon);

	PlayerInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::TryAbilityEnabled);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	PlayerInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<1>);
	PlayerInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<2>);
	PlayerInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<3>);
	PlayerInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<4>);
	PlayerInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<5>);
	PlayerInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<6>);
	PlayerInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<7>);
	PlayerInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<8>);
	PlayerInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<9>);
	PlayerInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<0>);
}

bool ATopDownShooterCharacter::TrySwitchWeaponToIndexByKeyInput(int32 ToIndex)
{
	
	if (CurrentWeapon && !(CurrentWeapon->bIsWeaponReloading) && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->bIsWeaponReloading)
				{
					CurrentWeapon->CancelReloading();
				}
			}
			
			return InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);

		}
	}
	
	return false;
}


void ATopDownShooterCharacter::InputAxisX(float Value)
{ 
	AxisX = Value;
}


void ATopDownShooterCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}


void ATopDownShooterCharacter::InputAttackPressed()
{
	if (bIsAlive)
	{
		AttackCharEvent(true);
	}
	
}


void ATopDownShooterCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}


void ATopDownShooterCharacter::MovementTick(float DeltaTime)
{	
	if (bIsAlive)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

		APlayerController* MyController = UGameplayStatics::GetPlayerController(this, 0);

		if (MyController)
		{
			FHitResult HitResult;
			MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, false, HitResult);
			
		
			
			
			
			SetActorRotation(FQuat(FRotator(0.0f, UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw, 0.0f)));
			/*
			if (GetActorRotation().Yaw > 90)
			{
				SetActorRotation(FQuat(FRotator(0.0f, 90.0f, 0.0f)));
			}
			
			if (GetActorRotation().Yaw < -90)set 
			{
				SetActorRotation(FQuat(FRotator(0.0f, -90.0f, 0.0f)));
			}*/


			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);

				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 134.0f);
					CurrentWeapon->ShouldReduseDispersion = true;
					break;

				case EMovementState::AimWalk_State:
					Displacement = FVector(0.0f, 0.0f, 134.0f);
					CurrentWeapon->ShouldReduseDispersion = true;
					break;

				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduseDispersion = false;
					break;

				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduseDispersion = false;
					break;


				case EMovementState::SprintRun_State:
					break;

				default:
					break;

				}

				CurrentWeapon->ShootEndLocation = HitResult.Location + Displacement;

			}
		}
	}
}


void ATopDownShooterCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATopDownShooterCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}


void ATopDownShooterCharacter::CharacterUpdate()
{	
	float ResSpeed = MovementSpeedInfo.RunSpeedNormal;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementSpeedInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementSpeedInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementSpeedInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementSpeedInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementSpeedInfo.SprintRunSpeedRun;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;

}


void ATopDownShooterCharacter::ChangeMovementState()
{	
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
		if (CurrentWeapon)
		{
			GetCurrentWeapon()->WeaponAiming = false;
		}
	
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled   = false;
			AimEnabled    = false;
			if (CurrentWeapon)
			{
				GetCurrentWeapon()->WeaponAiming = false;
			}
				
			MovementState = EMovementState::SprintRun_State;
		}
		else if (WalkEnabled && !SprintRunEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
			if (CurrentWeapon)
			{
				GetCurrentWeapon()->WeaponAiming = true;
			}
				
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
				if (CurrentWeapon)
				{
					GetCurrentWeapon()->WeaponAiming = false;
				}
				
			}
			else
			{
				if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
					if (CurrentWeapon)
					{
						GetCurrentWeapon()->WeaponAiming = true;
					}
				}
			}
		}
	}

	CharacterUpdate();

	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}


AWeaponDefault* ATopDownShooterCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}


void ATopDownShooterCharacter::TryReloadWeapon()
{
	if (bIsAlive && CurrentWeapon->bIsWeaponReloading == false)
	{
		if (CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CanWeaponReload())
		{
			CurrentWeapon->InitReload();
		}

	}
}


void ATopDownShooterCharacter::TrySwitchPrevWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->bIsWeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->bIsWeaponReloading)
				CurrentWeapon->CancelReloading();
		}

		if (InventoryComponent)
		{

			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			
			}



		}

	}
}


void ATopDownShooterCharacter::TrySwitchNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->bIsWeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->bIsWeaponReloading)
				CurrentWeapon->CancelReloading();
		}

		if (InventoryComponent)
		{

			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
			}



		}

	}
}

void ATopDownShooterCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)
	{
		UStateEffect* NewEffect = NewObject<UStateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}


void ATopDownShooterCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 RightIndex)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}



	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;

	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));

				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					
					CurrentWeapon = myWeapon;
					CurrentIndexWeapon = RightIndex;
					

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);
				
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATopDownShooterCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponFireStart);
				
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CanWeaponReload())
					{
						CurrentWeapon->InitReload();
					}

					if(InventoryComponent)
						InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);

				}
			}

		

		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATopDownShooterCharacter::InitWeapon - Weapon not found in table -NULL"));
		}

	}

	
	
}


void ATopDownShooterCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}


void ATopDownShooterCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	


	WeaponReloadEnd_BP(bIsSuccess);
}


void ATopDownShooterCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{

}


void ATopDownShooterCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{

}


void ATopDownShooterCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if(InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);

	WeaponFireStart_BP(Anim);
}


void ATopDownShooterCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{

}


UDecalComponent* ATopDownShooterCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

float ATopDownShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{	


	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
		
	if (bIsAlive)
	{
		CharHealthComponent->ChangeHealthValue(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		
		if (myProjectile)
		{	
			if (CharHealthComponent->GetCurrentHealth() < CurrentHealth)
			{
				CurrentHealth = CharHealthComponent->GetCurrentHealth();
				UTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
			}
				
		}
	
	
	}






	return ActualDamage;
}

void ATopDownShooterCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int8 rnd = FMath::RandHelper(AnimsOfDeath.Num());

	if (AnimsOfDeath.IsValidIndex(rnd) && AnimsOfDeath[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = AnimsOfDeath[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(AnimsOfDeath[rnd]);
	}
	
	bIsAlive = false;

	if (GetController())
	{
		GetController()->UnPossess();
	}

	//UnPossessed();

	AttackCharEvent(false);

	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATopDownShooterCharacter::EnableRagDoll, TimeAnim, false);
	
	GetCursorToWorld()->SetVisibility(false);

	CharDead_BP();
}

void ATopDownShooterCharacter::EnableRagDoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}

	

	

}



void ATopDownShooterCharacter::CharDead_BP_Implementation()
{
	//BP
}

EPhysicalSurface ATopDownShooterCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	if (GetMesh())
	{	
		

		if (CharHealthComponent)
		{
			if (CharHealthComponent->GetCurrentShield() <= 0.0f)
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);

				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;

					return Result;
				}
			}
		}
	}

	return Result;
}

TArray<UStateEffect*> ATopDownShooterCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATopDownShooterCharacter::RemoveEffect(UStateEffect* RemoveEffect)
{

	Effects.Remove(RemoveEffect);
}

void ATopDownShooterCharacter::AddEffect(UStateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}

UParticleSystemComponent* ATopDownShooterCharacter::SpawnParticleSystem(UParticleSystem* Particles)
{
	
	UParticleSystemComponent* ParticleSystem;

	


	ParticleSystem = UGameplayStatics::SpawnEmitterAttached(Particles, GetMesh(), NameToAttach);

	return ParticleSystem;
	
}

void ATopDownShooterCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		FDropItem ItemInfo;
		InventoryComponent->DropWeaponByIndex(CurrentIndexWeapon, ItemInfo);
	}
}


