

#include "Types.h"
#include "TopDownShooter.h"
#include "TDS_IGameInterface.h"

void UTypes::AddEffectBySurfaceType(AActor* EffectTaker, FName NameBoneHit, TSubclassOf<UStateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && EffectTaker && AddEffectClass)
	{
		UStateEffect* myEffect = Cast<UStateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;

			while (i < myEffect->PossibleIteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PossibleIteractSurface[i] == SurfaceType)
				{	
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;

					if (!myEffect->bIsStackeble)
					{
						int8 j = 0;
						ITDS_IGameInterface* myInterface = Cast<ITDS_IGameInterface>(EffectTaker);
						TArray<UStateEffect*> CurrentEffects;

						if (myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();

						}

						if (CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{

								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}

								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}
						
					}
				
					else
					{
						bIsCanAddEffect = true;
					}

					if (bIsCanAddEffect)
					{
						
						UStateEffect* NewEffect = NewObject<UStateEffect>(EffectTaker, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(EffectTaker, NameBoneHit);
						}
					}



				
				}

				i++;
			}
		}


	}

}



