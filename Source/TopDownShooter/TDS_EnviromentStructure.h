#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_IGameInterface.h"
#include "TDSHealthComponent.h"
#include "StateEffect.h"
#include "Components/ArrowComponent.h"
#include "Particles/ParticleSystemComponent.h"

#include "TDS_EnviromentStructure.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDS_EnviromentStructure : public AActor, public ITDS_IGameInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_EnviromentStructure();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAcess = true, Category = "Components"))
		UStaticMeshComponent* MeshComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAcess = true, Category = "Components"))
		USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAcess = true, Category = "Components"))
		UTDSHealthComponent* HealthComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		UArrowComponent* AttachLocation = nullptr;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	//Effects
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<UStateEffect*> Effects;
	
	FName NameToAttach = "AttachLocation";
	
	//Interface
	EPhysicalSurface GetSurfaceType() override;
	UParticleSystemComponent* SpawnParticleSystem(UParticleSystem* Particles) override;

	TArray<UStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UStateEffect* RemoveEffect) override;
	void AddEffect(UStateEffect* NewEffect) override;


};
