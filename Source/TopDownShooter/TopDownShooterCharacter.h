// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "WeaponDefault.h"
#include "TDSInventoryComponent.h"
#include "TDSCharacterHealthComponent.h"
#include "TopDownShooterPlayerController.h"
#include "TDS_IGameInterface.h"
#include "StateEffect.h"


#include "TopDownShooterCharacter.generated.h"

UCLASS(Blueprintable)
class ATopDownShooterCharacter : public ACharacter, public ITDS_IGameInterface
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(Id);
	}

public:
	ATopDownShooterCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDSInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDSCharacterHealthComponent* CharHealthComponent;

	FTimerHandle TimerHandle_RagDollTimer;


private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;




public:
	
	
	
	
	bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Cursor")
		FVector	CursorSize = FVector(20.0f, 40.0f, 40.0f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement" )
		EMovementState MovementState = EMovementState::Walk_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FChatacterSpeed MovementSpeedInfo;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool bIsAlive = true;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Animations")
		TArray<UAnimMontage*> AnimsOfDeath;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UStateEffect> AbilityEffect = nullptr;

	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;


	UDecalComponent* CurrentCursor = nullptr;


	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAxisY(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION(BlueprintCallable)
		void TrySwitchPrevWeapon();
	UFUNCTION(BlueprintCallable)
		void TrySwitchNextWeapon();
	UFUNCTION(BlueprintCallable)
		void TryAbilityEnabled();


	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UFUNCTION()
	void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 RightIndex);
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();

	

	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSucess, int32 AmmoSafe);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSucess);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);

	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);


	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	UPROPERTY(BlueprintReadOnly)
		int32 CurrentIndexWeapon = 0;

		virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	UFUNCTION(BlueprintCallable)
		void CharDead();
		void EnableRagDoll();

		UFUNCTION(BlueprintNativeEvent)
			void CharDead_BP();



	//Interface:

		EPhysicalSurface GetSurfaceType() override;
		TArray<UStateEffect*> GetAllCurrentEffects() override;
		void RemoveEffect(UStateEffect* RemoveEffect) override;
		void AddEffect(UStateEffect* NewEffect) override;
		
		float CurrentHealth = 100.0f;

		FName NameToAttach = "AttachSocket";

		UParticleSystemComponent* SpawnParticleSystem(UParticleSystem* Particles) override;

	//Effects
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		TArray<UStateEffect*> Effects;

	//Inventory

	 void DropCurrentWeapon();
};

