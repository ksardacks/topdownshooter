// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_EnviromentStructure.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
//#include "Materials/MaterialInerface.h"

// Sets default values
ATDS_EnviromentStructure::ATDS_EnviromentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetupAttachment(RootComponent);

	HealthComponent = CreateDefaultSubobject<UTDSHealthComponent>(TEXT("HealthComponent"));

	AttachLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("AttachLocation"));
	AttachLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATDS_EnviromentStructure::BeginPlay()
{
	Super::BeginPlay();
	

}

// Called every frame
void ATDS_EnviromentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATDS_EnviromentStructure::GetSurfaceType()
{	
	EPhysicalSurface mySurface = EPhysicalSurface::SurfaceType_Default;

	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			mySurface = myMaterial->GetPhysicalMaterial()->SurfaceType;

			 
		}


	}
	return mySurface;
}

UParticleSystemComponent* ATDS_EnviromentStructure::SpawnParticleSystem(UParticleSystem* Particles)
{
	UParticleSystemComponent* ParticleSystem;
	
	FVector Loc = AttachLocation->GetComponentLocation();
	

	ParticleSystem = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Particles, AttachLocation->GetComponentTransform(), false);

	return ParticleSystem;
}

TArray<UStateEffect*> ATDS_EnviromentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATDS_EnviromentStructure::RemoveEffect(UStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDS_EnviromentStructure::AddEffect(UStateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}
