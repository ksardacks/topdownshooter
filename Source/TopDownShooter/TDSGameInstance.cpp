// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInstance.h"

bool UTDSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;

	if (WeaponInfoTable)
	{

		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);

		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("TDSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}
	
	return bIsFind;
	
}


bool UTDSGameInstance::GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo)
{
	if (DropItemInfoTable)
	{
		FDropItem* DropItemInfoRow;
		TArray<FName>RowNames = DropItemInfoTable->GetRowNames();

		int8 i = 0;      

		while (i < RowNames.Num())
		{
			DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");

			if (DropItemInfoRow->WeaponInfo.NameItem == NameItem)
			{
				OutInfo = *DropItemInfoRow;
				return true;
			}

			i++;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("TDSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}

	return false;
}


bool UTDSGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
	if (DropItemInfoTable)
	{
		FDropItem* DropItemInfoRow;
		
		if (DropItemInfoTable)
		{
			DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(NameItem, "", false);

			if (DropItemInfoRow->WeaponInfo.NameItem == NameItem)
			{
				OutInfo = *DropItemInfoRow;
				return true;
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("TDSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}

	return false;
}