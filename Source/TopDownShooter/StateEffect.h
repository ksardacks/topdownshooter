#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"

#include "StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TOPDOWNSHOOTER_API UStateEffect : public UObject
{
	GENERATED_BODY()

public:

	virtual bool InitObject(AActor* Actor, FName NameBoneHit);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<TEnumAsByte<EPhysicalSurface>> PossibleIteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		bool bIsStackeble = false;

	AActor* myActor = nullptr;
};

UCLASS()
class TOPDOWNSHOOTER_API UStateEffect_ExecuteOnce : public UStateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor, FName NameBoneHit) override;

	void DestroyObject() override;

	virtual	void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 20.0f;
};


UCLASS()
class TOPDOWNSHOOTER_API UStateEffect_ExecuteTimer : public UStateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;
	virtual	void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
	
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	TArray<UParticleSystem*> PatricleEffects;
};
