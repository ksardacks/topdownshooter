#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "StateEffect.h"
#include "Types.h"


#include "TDS_IGameInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDS_IGameInterface : public UInterface
{
	GENERATED_BODY()
};


class TOPDOWNSHOOTER_API ITDS_IGameInterface
{
	GENERATED_BODY()

		// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UStateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UStateEffect* RemoveEffect);
	virtual void AddEffect(UStateEffect* NewEffect);

	FName NameToAttach;

	virtual UParticleSystemComponent* SpawnParticleSystem(UParticleSystem* Particles);


	//Inventory
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);

};
