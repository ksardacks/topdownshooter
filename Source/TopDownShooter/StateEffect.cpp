
#include "StateEffect.h"
#include "TDSCharacterHealthComponent.h"
#include "TDS_IGameInterface.h"
#include "Kismet/KismetMathLibrary.h"
#include "TopDownShooterCharacter.h"


#include "Kismet/GameplayStatics.h"


bool UStateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;

	ITDS_IGameInterface* myInterface = Cast<ITDS_IGameInterface>(myActor);

	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}

void UStateEffect::DestroyObject()
{	

	ITDS_IGameInterface* myInterface = Cast<ITDS_IGameInterface>(myActor);

	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;

	if(this && this->IsValidLowLevel())
		this->ConditionalBeginDestroy();
}


bool UStateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	
	ExecuteOnce();

	return true;
	
}

void UStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UStateEffect_ExecuteOnce::ExecuteOnce()
{	
	

	if (IsValid(Cast<ATopDownShooterCharacter>(myActor)))
	{
		if (Cast<ATopDownShooterCharacter>(myActor)->bIsAlive)
		{
			UTDSHealthComponent* myHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
			if (myHealthComp)
				myHealthComp->ChangeHealthValue(Power);
		}
		else
		{
			DestroyObject();
		}
	}
	else
	{
		if (myActor)
		{
			UTDSHealthComponent* myHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
			if (myHealthComp)
				myHealthComp->ChangeHealthValue(Power);
		}
	
		DestroyObject();
	}
	

	
}



bool UStateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UStateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{		
		
		ITDS_IGameInterface* myInterface = Cast<ITDS_IGameInterface>(myActor);
		
		if (myInterface)
		{	
			if (PatricleEffects.Num() > 0)
			{
				int8 IndexEffect = UKismetMathLibrary::RandomInteger64InRange(0, PatricleEffects.Num() - 1);

				if (PatricleEffects[IndexEffect])
				{
					ParticleEffect = PatricleEffects[IndexEffect];
				}
			}
			
			ParticleEmitter = myInterface->SpawnParticleSystem(ParticleEffect);
		}
		else
		{

			FName NameBoneToAttached = NameBoneHit;
			FVector Loc = FVector(0);

			USceneComponent* mySkelMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));

			if (mySkelMesh)
			{
				ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, mySkelMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
			else
			{
				ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}

			
		}
	
	}
	
	return true;
}

void UStateEffect_ExecuteTimer::DestroyObject()
{	
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;


	Super::DestroyObject();
}

void UStateEffect_ExecuteTimer::Execute()
{	
	if (IsValid(Cast<ATopDownShooterCharacter>(myActor)))
	{	
		if (Cast<ATopDownShooterCharacter>(myActor)->bIsAlive)
		{
			UTDSHealthComponent* myHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
			if (myHealthComp)
				myHealthComp->ChangeHealthValue(Power);
		}
		
	}
	else
	{
		if (myActor)
		{
			UTDSHealthComponent* myHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
			if (myHealthComp)
				myHealthComp->ChangeHealthValue(Power);
		}
	}


	

}
